Test case to reproduce issue w/ MiniMagick

# Run the app

```bash
bundle install --with contrast
bundle exec ruby app.rb
```


# Perform a file upload

1. Install [HTTPie](https://httpie.org/)
1. Do `http -f post :4567/file file@~/Some/image.png`
