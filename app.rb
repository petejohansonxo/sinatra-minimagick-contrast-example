require 'sinatra'
require 'mini_magick'

require 'contrast-agent'

use Contrast::Agent::Middleware, true

post '/file' do
  file = params['file']['tempfile']
  p 'About to open image'
  img = MiniMagick::Image.read(file)
  p 'Openned the image!'
  'Success'
end
